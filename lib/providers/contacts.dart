import 'package:flutter/foundation.dart';

class Contacts {
  final String id;
  final String name;
  final List<String> numbers;
  final bool isHaveApp;
  final String countyCode; //todo ENUM

  Contacts({
    @required this.id,
    @required this.name,
    @required this.numbers,
    this.isHaveApp = false,
    this.countyCode,
  });
}
