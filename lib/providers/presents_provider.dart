import 'dart:async';
import 'dart:convert';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/models/present.dart';

class PresentProvider with ChangeNotifier {
  List<Present> _items = [];

  final String token;
  final String mobTel;

  PresentProvider(this.token, this.mobTel, this._items);

  List<Present> get items {
    return [..._items];
  }

  List<Present> wishesFromGroup(GROUPS groups) {
    if (groups == GROUPS.ALL) {
      return _items;
    } else {
      return _items.where((wish) => wish.groups == groups).toList();
    }
  }

  int getLengthOfPresents() {
    return _items.length;
  }

  List<Present> presentForMember(String idMember) {
    return _items.where((present) => present.idMember == idMember).toList();
  }

  findById(String id) {
    return _items.firstWhere((wish) => wish.id == id);
  }

  Future<void> fetchAndSetPresents() async {
    final List<Present> loadedPresents = [];
    var deeper =
        FirebaseDatabase.instance.reference().child('$mobTel').child('members');
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .onValue
        .listen((Event event) {
      for (var keyM in event.snapshot.value.keys) {
        deeper.child(keyM).child('presents').onValue.listen((Event event) {
          _items.clear();
          for (var key in event.snapshot.value.keys) {
            loadedPresents.add(Present(
              id: key,
              idMember: event.snapshot.value[key]['MemberID'],
              name: event.snapshot.value[key]['Name'],
              dateOfAdd: event.snapshot.value[key]['DateOfAdd'],
              groups: event.snapshot.value[key]['Groups'] == 'GROUPS.FRIENDS'
                  ? GROUPS.FRIENDS
                  : event.snapshot.value[key]['Groups'] == 'GROUPS.FAMILY'
                      ? GROUPS.FAMILY
                      : event.snapshot.value[key]['Groups'] ==
                              'GROUPS.COLLEAGUES'
                          ? GROUPS.COLLEAGUES
                          : GROUPS.MYSELF,
              description: event.snapshot.value[key]['Description'],
              image: event.snapshot.value[key]['Image'],
            ));
          }
        });
      }
    });
    _items = loadedPresents;
  }

  Future<void> fetchAndSetMyPresents(String mobileNr) async {
    final List<Present> loadedPresents = [];
    FirebaseDatabase.instance
        .reference()
        .child('$mobileNr')
        .child('myPresents')
        .onValue
        .listen((Event event) {
      _items.clear();
      for (var key in event.snapshot.value.keys) {
        loadedPresents.add(Present(
          id: key,
          idMember: event.snapshot.value[key]['MemberID'],
          name: event.snapshot.value[key]['Name'],
          dateOfAdd: event.snapshot.value[key]['DateOfAdd'],
          groups: event.snapshot.value[key]['Groups'] == 'GROUPS.FRIENDS'
              ? GROUPS.FRIENDS
              : event.snapshot.value[key]['Groups'] == 'GROUPS.FAMILY'
                  ? GROUPS.FAMILY
                  : event.snapshot.value[key]['Groups'] == 'GROUPS.COLLEAGUES'
                      ? GROUPS.COLLEAGUES
                      : GROUPS.MYSELF,
          description: event.snapshot.value[key]['Description'],
          image: event.snapshot.value[key]['Image'],
        ));
      }
    });
    _items = loadedPresents;
  }

  Future<void> addPresent(Present present) {
    _items.add(present);
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .child(present.idMember)
        .child('presents')
        .push()
        .set(present.toJson());
  }

  Future<void> addMyPresent(Present myPresent) async {
    myPresent.idMember = mobTel;
    _items.add(myPresent);
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('myPresents')
        .push()
        .set(myPresent.toJson());
  }

  void updatePresent(Present newPresent) {
    _items[_items.indexWhere((present) => present.id == newPresent.id)] =
        newPresent;
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .child(newPresent.idMember)
        .child('presents')
        .child(newPresent.id)
        .set(newPresent.toJson());
  }

  void deletePresent(String presentId, String memberId) {
    _items.removeWhere((present) => present.id == presentId);
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .child(memberId)
        .child('presents')
        .child(presentId)
        .remove();
  }
}
