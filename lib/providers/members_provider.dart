import 'dart:convert';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_app_app/models/member.dart';

class MembersProvider with ChangeNotifier {
  List<Member> _items = [];

  final String token;
  final String mobTel;

  MembersProvider(this.token, this.mobTel, this._items);

  List<Member> get items {
    return [..._items];
  }

  List<Member> memberFromGroup(GROUPS groups) {
    if (groups == GROUPS.ALL) {
      return _items.where((member) => member.groups != GROUPS.MYSELF).toList();
    } else {
      return _items.where((member) => member.groups == groups).toList();
    }
  }

  int getLengthOfMembers() {
    return _items.length;
  }

  findByPhoneCountryNumber(String phoneNumber) {
    if (phoneNumber != '') {
      return _items.firstWhere((member) => member.id == phoneNumber);
    }
  }

  findById(String memberId) {
    return _items.firstWhere((member) => member.id == memberId);
  }

  findByPhoneNumber(String phoneNumber) {
    return _items.firstWhere((member) => member.phoneNumber == phoneNumber);
  }

  final FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://flutter-wishapp.appspot.com');

  Future<void> fetchAndSetMembers() async {
    final List<Member> loadedMembers = [];
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .onValue
        .listen((Event event) {
      _items.clear();
      for (var key in event.snapshot.value.keys) {
        loadedMembers.add(Member(
          id: key,
          phoneNumber: event.snapshot.value[key]['PhoneNumber'],
          name: event.snapshot.value[key]['Name'],
          dateOfBD: event.snapshot.value[key]['DateOfBD'],
          groups: event.snapshot.value[key]['Groups'] == 'GROUPS.FRIENDS'
              ? GROUPS.FRIENDS
              : event.snapshot.value[key]['Groups'] == 'GROUPS.FAMILY'
                  ? GROUPS.FAMILY
                  : GROUPS.COLLEAGUES,
          isHaveApp: false,
          image: event.snapshot.value[key]['Image'],
        ));
      }
    });
    _items = loadedMembers;
  }

  Future<void> addMember(Member member) {
    _items.add(member);
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .child(member.phoneNumber)
        .set(member.toJson());
  }

  Future<void> updateMember(Member newMember) async {
    _items[_items.indexWhere((member) => member.id == newMember.id)] =
        newMember;
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .child(newMember.id)
        .set(newMember.toJson());
  }

  void deleteMember(String id) {
    _items.removeWhere((member) => member.id == id);
    FirebaseDatabase.instance
        .reference()
        .child('$mobTel')
        .child('members')
        .child(id)
        .remove();
  }
}
