import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';
import 'package:flutter_app_app/screens/auth_screen.dart';
import 'package:flutter_app_app/screens/bottom_tabs.dart';
import 'package:flutter_app_app/screens/home_screen.dart';
import 'package:flutter_app_app/screens/member_edit_screen.dart';
import 'package:flutter_app_app/screens/members_screen.dart';
import 'package:flutter_app_app/screens/present_detail_screen.dart';
import 'package:flutter_app_app/screens/present_edit_screen.dart';
import 'package:provider/provider.dart';

import 'models/auth.dart';
import 'providers/members_provider.dart';

import 'screens/member_detail_screen.dart';
import 'screens/contacts_screen.dart';
import 'screens/member_creating_screen.dart';
import 'screens/present_creating_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  FirebaseUser user;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        ChangeNotifierProxyProvider<Auth, MembersProvider>(
          update: (ctx, auth, previousMemberProvider) => MembersProvider(
              auth.token,
              auth.mobileNumber,
              previousMemberProvider == null
                  ? []
                  : previousMemberProvider.items),
        ),
        ChangeNotifierProxyProvider<Auth, PresentProvider>(
          update: (ctx, auth, previousWishProvider) => PresentProvider(
              auth.token,
              auth.mobileNumber,
              previousWishProvider == null ? [] : previousWishProvider.items),
        ),
      ],
      child: Consumer<Auth>(builder: (ctx, auth, _) {
        return MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            primaryColor: Colors.black,
            buttonColor: Colors.black,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
          ),
          home: auth.token != null ? BottomTabs() : AuthScreen(),
          routes: {
            MemberDetailScreen.routeName: (ctx) => MemberDetailScreen(),
            ContactsToInviteScreen.routeName: (ctx) => ContactsToInviteScreen(),
            MemberCreatingScreen.routeName: (ctx) => MemberCreatingScreen(),
            PresentCreatingScreen.routeName: (ctx) => PresentCreatingScreen(),
            PresentDetailScreen.routeName: (ctx) => PresentDetailScreen(),
            MemberEditingScreen.routeName: (ctx) => MemberEditingScreen(),
            PresentEditingScreen.routeName: (ctx) => PresentEditingScreen(),
          },
        );
      }),
    );
  }
}
