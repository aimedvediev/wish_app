import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_app_app/models/member.dart';

class Present {
  String id;
  String name;
  String image;
  String dateOfAdd;
  String idMember;
  String description;
  GROUPS groups;

  Present({
    this.id,
    this.name,
    this.image,
    this.dateOfAdd,
    this.idMember,
    this.description,
    this.groups,
  });

  toJson() {
    return {
      'PresentID': id,
      'MemberID': idMember,
      'Name': name,
      'DateOfAdd': dateOfAdd,
      'Groups': groups.toString(),
      'Description': description,
      'Image': image,
    };
  }

  Present.fromSnapshot(DataSnapshot snapshot)
      : id = snapshot.key,
        idMember = snapshot.value['MemberID'],
        name = snapshot.value['Name'],
        dateOfAdd = snapshot.value['DateOfAdd'],
        groups = snapshot.value['Groups'],
        description = snapshot.value['Description'],
        image = snapshot.value['Image'];
}
