import 'dart:convert';
import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class Auth with ChangeNotifier {
  String _token;
  String _mobileNumber = '000';

  Future<void> takeAuth() async {
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();
    _mobileNumber = currentUser.phoneNumber;
    _token = currentUser.uid;
    notifyListeners();
  }

  String get mobileNumber => _mobileNumber;

  String get token => _token;

  set mobileNumber(String value) {
    _mobileNumber = value;
  }

  set token(String value) {
    _token = value;
  }


}
