import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';

enum GROUPS { ALL, FAMILY, FRIENDS, COLLEAGUES, MYSELF }

class Member {
  String id;
  String phoneNumber;
  String name;
  String dateOfBD;
  GROUPS groups;
  bool isHaveApp;
  String image;

  Member({
    this.id,
    this.phoneNumber,
    this.name,
    this.dateOfBD,
    this.groups,
    this.isHaveApp,
    this.image,
  });

  toJson() {
    return {
      'MemberID': id,
      'PhoneNumber': phoneNumber,
      'Name': name,
      'DateOfBD': dateOfBD,
      'Groups': groups.toString(),
      'IsHaveApp': isHaveApp,
      'Image': image,
    };
  }

  Member.fromSnapshot(DataSnapshot snapshot)
      : id = snapshot.key,
        phoneNumber = snapshot.value['PhoneNumber'],
        name = snapshot.value['Name'],
        dateOfBD = snapshot.value['DateOfBD'],
        groups = snapshot.value['Groups'] =='GROUPS.FRIENDS'
              ? GROUPS.FRIENDS
              : snapshot.value['Groups'] == 'GROUPS.FAMILY'
                  ? GROUPS.FAMILY
                  : GROUPS.COLLEAGUES,
        isHaveApp = snapshot.value['IsHaveApp'],
        image = snapshot.value['Image'];

  String toString() => name;

  operator ==(o) => o is Member && o.phoneNumber == phoneNumber;

  int get hashCode => phoneNumber.hashCode ^ name.hashCode ^ dateOfBD.hashCode;
}
