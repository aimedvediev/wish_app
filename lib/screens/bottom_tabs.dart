import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_app/screens/members_screen.dart';
import 'package:flutter_app_app/screens/presents_list_screen.dart';
import 'package:flutter_app_app/screens/home_screen.dart';

class BottomTabs extends StatefulWidget {

  @override
  _BottomTabsState createState() => _BottomTabsState();
}

class _BottomTabsState extends State<BottomTabs> {


  List<Widget> _pages = [
    MemberScreen(),
    HomeScreen(),
    ListOfPresentScreen(),
  ];

  int _selectedPageIndex = 1;



  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_selectedPageIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedPageIndex,
        onTap: _selectPage,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        iconSize: 32,
        items: [
          BottomNavigationBarItem(
            title: Text('Member'),
            icon: Icon(Icons.people),
          ),
          BottomNavigationBarItem(
            title: Text('Home'),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            title: Text('Wish'),
            icon: Icon(Icons.list),
          ),
        ],
      ),
    );
  }
}
