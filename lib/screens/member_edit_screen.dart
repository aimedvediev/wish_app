import 'dart:io';

import 'package:country_pickers/country.dart';
import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;
import 'package:firebase_storage/firebase_storage.dart';

import 'package:flutter/material.dart';

import 'package:flutter_app_app/widgets/chip_category.dart';
import 'package:flutter_app_app/widgets/title_creating.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:provider/provider.dart';

class MemberEditingScreen extends StatefulWidget {
  static const routeName = '/member_editing';

  @override
  _MemberEditingScreenState createState() => _MemberEditingScreenState();
}

class _MemberEditingScreenState extends State<MemberEditingScreen> {
  Color colorForFamily = Colors.black;

  File _imageFile;
  String _filePath;
  StorageUploadTask _uploadTask;

  var _isLoading = false;

  var isChangeImage = false;

  var _isInit = true;

  List<GROUPS> groupsList = [
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.FAMILY;

  DateTime _dateTime;
  static Country country = CountryPickerUtils.getCountryByIsoCode('PL');

  final _form = GlobalKey<FormState>();
  var _editedMember = Member(
    name: '',
    phoneNumber: '123',
    dateOfBD: '1212',
    groups: GROUPS.FAMILY,
    image:
        'https://cdn.cnn.com/cnnnext/dam/assets/191024091949-02-foster-cat-large-169.jpg',
  );

  var _initValues = {
    'countryNumber': '',
    'name': '',
    'phoneNumber': '',
    'dateofBD': '',
    'groups': '',
    'imageUrl': '',
  };

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      Provider.of<MembersProvider>(context, listen: false)
          .updateMember(_editedMember);
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('An error occurred'),
          content: Text(error.toString()),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } finally {
      setState(() {
        //if (isChangeImage) _startUpload();
        _isLoading = false;
      });
      Navigator.of(context).pop();
    }
  }

  final FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://flutter-wishapp.appspot.com');

  Future<void> _pickImage(ImageSource source) async {
    File selected = await ImagePicker.pickImage(source: source);

    final appDir = await syspath.getApplicationDocumentsDirectory();
    final fileName = path.basename(selected.path);
    final savedImage = await selected.copy('${appDir.path}/$fileName');

    setState(() {
      _filePath = '${DateTime.now()}${_editedMember.phoneNumber}.png';
      _imageFile = selected;

      if (_imageFile != null) {
        isChangeImage = true;

        _editedMember = Member(
            name: _editedMember.name,
            //todo phonenumber without country code
            phoneNumber: _editedMember.phoneNumber,
            dateOfBD: _editedMember.dateOfBD,
            groups: _editedMember.groups,
            image: savedImage.path,
            id: _editedMember.id);
      }
    });
  }

  Future<void> _startUpload() async {
    setState(() {
      _uploadTask =
          _storage.ref().child('images/Members/$_filePath').putFile(_imageFile);
    });
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final memberId = ModalRoute.of(context).settings.arguments as String;
      _editedMember = Provider.of<MembersProvider>(context, listen: false)
          .findById(memberId);
      _initValues = {
        'name': _editedMember.name,
        'phoneNumber': _editedMember.phoneNumber,
        'dateOfBD': _editedMember.dateOfBD,
        'groups': _editedMember.groups.toString(),
        'imageUrl': _editedMember.image,
      };
      selectedGroup = toChangeGroups(_editedMember.groups.toString());
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _saveForm,
        label: Text(
          'SAVE',
          style: TextStyle(
            color: Color(0xFF222d6d),
            fontSize: 16,
          ),
        ),
        backgroundColor: Color(0xFFf4f1ea),
      ),
      backgroundColor: Color(0xFFffffff),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.only(
                left: 50,
                top: 70,
                right: 50,
              ),
              child: Form(
                key: _form,
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Text(
                        'Edit member',
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF222d6d),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'MOBILE NUMBER',
                          ),
                          Row(
                            children: <Widget>[
                              FlatButton(
                                padding: EdgeInsets.only(top: 15),
                                child: Row(
                                  children: <Widget>[
                                    CountryPickerUtils.getDefaultFlagImage(
                                        country),
                                    SizedBox(
                                      width: 8.0,
                                    ),
                                    Text("+${country.phoneCode}"),
                                  ],
                                ),
                                onPressed: _openCountryPickerDialog,
                              ),
                              Expanded(
                                child: TextFormField(
                                  initialValue: _initValues['phoneNumber'],
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(
                                    color: Color(0xFF000000),
                                    fontSize: 16,
                                  ),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    labelText: 'Enter phone number',
                                    hasFloatingPlaceholder: false,
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please provide a number';
                                    }
                                    if (value.length != 9) {
                                      return 'Incorect length of number';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    _editedMember = Member(
                                        name: _editedMember.name,
                                        phoneNumber: value,
                                        dateOfBD: _editedMember.dateOfBD,
                                        groups: _editedMember.groups,
                                        image: _editedMember.image,
                                        id: _editedMember.id);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'Name',
                          ),
                          TextFormField(
                            initialValue: _initValues['name'],
                            keyboardType: TextInputType.text,
                            style: TextStyle(
                              color: Color(0xFF000000),
                              fontSize: 16,
                            ),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              labelText: 'Enter person\'s name',
                              hasFloatingPlaceholder: false,
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please provide a name';
                              }
                              if (value.length < 1) {
                                return 'Must be at least 2';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              _editedMember = Member(
                                  name: value,
                                  phoneNumber: _editedMember.phoneNumber,
                                  dateOfBD: _editedMember.dateOfBD,
                                  groups: _editedMember.groups,
                                  image: _editedMember.image,
                                  id: _editedMember.id);
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'BIRTHDAY DATE',
                          ),
                          DatePickerWidget(
                            dateFormat: 'dd|MMMM|yyyy',
                            initialDateTime:
                                DateTime.parse(_initValues['dateOfBD']),
                            onChange: (dateTime, selectedIndex) {
                              setState(() {
                                _dateTime = dateTime;
                                _editedMember = Member(
                                    name: _editedMember.name,
                                    phoneNumber: _editedMember.phoneNumber,
                                    dateOfBD:
                                        '${_dateTime.year}-${_dateTime.month}-${_dateTime.day}',
                                    groups: _editedMember.groups,
                                    image: _editedMember.image,
                                    id: _editedMember.id);
                              });
                            },
                            pickerTheme: DateTimePickerTheme(
                              showTitle: false,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: TextTitle(
                              'PHOTO',
                            ),
                          ),
                          _initValues['imageUrl'] == null
                              ? Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 0.5,
                                      color: Colors.grey,
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                  ),
                                  height: 50,
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.add,
                                      size: 12,
                                      color: Colors.grey,
                                    ),
                                    onPressed: () =>
                                        _pickImage(ImageSource.gallery),
                                  ),
                                )
                              : ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(6),
                                  ),
                                  child: FlatButton(
                                    child: _imageFile != null
                                        ? Image.file(
                                            _imageFile,
                                            height: 50,
                                            width: 50,
                                            fit: BoxFit.cover,
                                          )
                                        : Image.file(
                                            File(_initValues['imageUrl']),
                                            height: 50,
                                            width: 50,
                                            fit: BoxFit.cover,
                                          ),
                                    onPressed: () =>
                                        _pickImage(ImageSource.gallery),
                                  ),
                                ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(),
                      height: 70,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'CATEGORY',
                          ),
                          Container(
                            height: 50,
                            padding: EdgeInsets.only(top: 10),
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    ChipCategory(
                                      initialCategory: selectedGroup,
                                      groups: groupsList,
                                      onSelectionChip: (selected) {
                                        setState(
                                          () {
                                            selectedGroup = selected;
                                            _editedMember = Member(
                                                name: _editedMember.name,
                                                phoneNumber:
                                                    _editedMember.phoneNumber,
                                                dateOfBD:
                                                    _editedMember.dateOfBD,
                                                groups: selected,
                                                image: _editedMember.image,
                                                id: _editedMember.id);
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
          data: Theme.of(context).copyWith(primaryColor: Colors.pink),
          child: CountryPickerDialog(
            titlePadding: EdgeInsets.all(8.0),
            searchCursorColor: Colors.pinkAccent,
            searchInputDecoration: InputDecoration(hintText: 'Search...'),
            isSearchable: true,
            title: Text('Select your phone code'),
            onValuePicked: (Country pickedCountry) {
              setState(() {
                country = pickedCountry;
                _editedMember = Member(
                    name: _editedMember.name,
                    phoneNumber: '+${pickedCountry.phoneCode}' +
                        _editedMember.phoneNumber,
                    dateOfBD: _editedMember.dateOfBD,
                    groups: GROUPS.COLLEAGUES,
                    image: _editedMember.image,
                    id: _editedMember.id);
              });
            },
            itemBuilder: _buildDialogItem,
          ),
        ),
      );

  Widget _buildDialogItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );

  GROUPS toChangeGroups(String groups) {
    return groups == 'GROUPS.FRIENDS'
        ? GROUPS.FRIENDS
        : groups == 'GROUPS.FAMILY' ? GROUPS.FAMILY : GROUPS.COLLEAGUES;
  }
}
