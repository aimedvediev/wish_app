import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';
import 'package:flutter_app_app/screens/present_detail_screen.dart';
import 'package:flutter_app_app/screens/member_edit_screen.dart';
import 'package:flutter_app_app/widgets/group_chip_member.dart';
import 'package:provider/provider.dart';

import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_app_app/widgets/present_item.dart';

class MemberDetailScreen extends StatefulWidget {
  static const routeName = '/member_detail';

  @override
  _MemberDetailScreenState createState() => _MemberDetailScreenState();
}

class _MemberDetailScreenState extends State<MemberDetailScreen> {
  bool myPresentList = true;
  bool membersPresentList = false;

  var _isInit = true;
  var _isLoading = false;

  void initState() {
    super.initState();
  }

  void didChangeDependencies() {
    if (myPresentList == true) {
      if (_isInit) {
        setState(() {
          _isLoading = true;
        });
        Provider.of<PresentProvider>(context).fetchAndSetPresents().then((_) {
          setState(() {
            _isLoading = false;
          });
        });
      }
      _isInit = false;
    }
    if (membersPresentList == true) {
      final memberId = ModalRoute.of(context).settings.arguments as String;
      final loadedMember = Provider.of<MembersProvider>(context, listen: false)
          .findById(memberId);
      if (_isInit) {
        setState(() {
          _isLoading = true;
        });
        Provider.of<PresentProvider>(context)
            .fetchAndSetMyPresents(loadedMember.phoneNumber)
            .then((_) {
          setState(() {
            _isLoading = false;
          });
        });
      }
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final memberId = ModalRoute.of(context).settings.arguments as String;
    final loadedMember =
        Provider.of<MembersProvider>(context, listen: false).findById(memberId);

    final wishesData = Provider.of<PresentProvider>(context);
    final wishes = wishesData.presentForMember(loadedMember.phoneNumber);
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color(0xFFf4f1ea),
            iconTheme: IconThemeData(color: Colors.grey),
            expandedHeight: 270,
            pinned: true,
            floating: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Padding(
                padding: const EdgeInsets.only(left: 75, right: 90, top: 52),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            child: Image.file(
                              File(loadedMember.image),
                              height: 104,
                              width: 104,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 12),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    loadedMember.name,
                                    style: TextStyle(
                                      color: Color(0xFF222d6d),
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Text(
                                    loadedMember.phoneNumber,
                                    style: TextStyle(
                                      color: Color(0xFF1a1824),
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 14,
                                  ),
                                  ChipGroupMember(loadedMember.groups),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    '+ Invite to the app',
                                    style: TextStyle(
                                      color: Color(0xFF222d6d),
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 0, top: 30),
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                Radius.circular(16),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                height: 48,
                                width: 244,
                                color: Color(0xFFffffff),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      'Birthday:',
                                      style: TextStyle(
                                        color: Color(0xFF222d6d),
                                      ),
                                    ),
                                    Text(
                                      loadedMember.dateOfBD,
                                      style: TextStyle(
                                        color: Color(0xFF222d6d),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          FlatButton(
                            child: Text('EDIT'),
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                  MemberEditingScreen.routeName,
                                  arguments: memberId);
                            },
                          ),
                          FlatButton(
                              child: Text('DELETE'),
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title:
                                            Text('Delete ${loadedMember.name}'),
                                        content: Text('Are you sure?'),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('Yes'),
                                            onPressed: () {
                                              Provider.of<MembersProvider>(
                                                      context,
                                                      listen: false)
                                                  .deleteMember(memberId);
                                              Navigator.of(context).pop();
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          FlatButton(
                                            child: Text('No'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                      );
                                    });
                              }),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(left: 50, top: 20, bottom: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  //todo widget
                  FlatButton(
                    child: Text(
                      'MY LIST FOR ${loadedMember.name}(${wishes.length})',
                      style: TextStyle(
                        fontSize: myPresentList ? 12 : 10,
                        color: myPresentList
                            ? Color(0xFF222d6d)
                            : Color(0xFFb4b4b4),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        myPresentList = true;
                        membersPresentList = false;
                        _isInit = true;
                        didChangeDependencies();
                      });
                    },
                  ),
                  //todo widget
                  FlatButton(
                    child: Text(
                      '${loadedMember.name} PRESENT LIST(${wishes.length})',
                      style: TextStyle(
                        fontSize: membersPresentList ? 12 : 10,
                        color: membersPresentList
                            ? Color(0xFF222d6d)
                            : Color(0xFFb4b4b4),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        myPresentList = false;
                        membersPresentList = true;
                        _isInit = true;
                        _isLoading = true;
                        didChangeDependencies();
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          _isLoading
              ? SliverToBoxAdapter(
                  child: CircularProgressIndicator(),
                )
              : SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return FlatButton(
                        child: WishItem(
                          wishes[index].id,
                          wishes[index].name,
                          '',
                          wishes[index].dateOfAdd,
                          wishes[index].image,
                          wishes[index].groups,
                          isYouCanDelete: myPresentList,
                        ),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                            PresentDetailScreen.routeName,
                            arguments: wishes[index].id,
                          );
                        },
                      );
                    },
                    childCount: wishes.length,
                  ),
                ),
        ],
      ),
    );
  }
}
