import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/providers/members_provider.dart';

import 'package:flutter_app_app/screens/contacts_screen.dart';
import 'package:flutter_app_app/screens/member_creating_screen.dart';

//import 'package:contacts_service/contacts_service.dart';

import 'package:flutter_app_app/widgets/fab.dart';

import 'package:flutter_app_app/widgets/chips_filter.dart';
import 'package:flutter_app_app/widgets/members_listview.dart';
import 'package:provider/provider.dart';

class MemberScreen extends StatefulWidget {
  @override
  _MemberScreenState createState() => _MemberScreenState();
}

class _MemberScreenState extends State<MemberScreen>
    with SingleTickerProviderStateMixin {
  List<GROUPS> groupsList = [
    GROUPS.ALL,
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.ALL;

  Animation<double> _animation;
  AnimationController _controller;

  var _isInit = true;
  var _isLoading = false;

  //List<Contact> _contacts;

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _controller);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    super.initState();
    //refreshContacts();
  }

  refreshContacts() async {
    //var contacts = (await ContactsService.getContacts()).toList();

    setState(() {
      // _contacts = contacts;
    });
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<MembersProvider>(context).fetchAndSetMembers().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final membersLength =
        Provider.of<MembersProvider>(context).getLengthOfMembers();

    return Scaffold(
      backgroundColor: Color(0xFFf4f1ea),
      floatingActionButton: ExpandedAnimationFab(
        items: [
          FabItem(
            'Create new member',
            Color(0xFF8794e3),
            onPress: () {
              _controller.reverse();
              Navigator.of(context).pushNamed(MemberCreatingScreen.routeName);
            },
          ),
          FabItem(
            'Invite from contact list',
            Color(0xFF76afd9),
            onPress: () {
              _controller.reverse();
              Navigator.of(context).pushNamed(ContactsToInviteScreen.routeName);
            },
          ),
        ],
        animation: _animation,
        onPress: () {
          if (_controller.isCompleted) {
            _controller.reverse();
          } else {
            _controller.forward();
          }
        },
      ),
      //-------
//      body: _contacts != null
//          ? ListView.builder(
//              itemCount: _contacts.length,
//              itemBuilder: (_, i) {
//                Contact c = _contacts?.elementAt(i);
//                final Iterable<Item> _items = c.phones;
//                return FlatButton(
//                  child: Column(
//                    children: _items
//                        .map(
//                          (i) => ListTile(
//                            title: Text(c.displayName ?? ""),
//                            subtitle: Text(i.value ?? ""),
//                          ),
//                        )
//                        .toList(),
//                  ),
//                  onPressed: () {},
//                );
//              },
//            )
//          : Center(
//              child: CircularProgressIndicator(),
//            ),
      //-------------
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                  top: 80,
                  left: 45,
                  right: 15,
                ),
                child: Text(
                  "$membersLength members are on your list:",
                  style: TextStyle(
                    color: Color(0xFF222d6d),
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20, bottom: 5, left: 25),
                height: 70,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    ChipsFilter(
                      groups: groupsList,
                      onSelectionChip: (selected) {
                        setState(
                          () {
                            selectedGroup = selected;
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                  ),
                  child: _isLoading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : MembersListView(
                          groups: selectedGroup,
                        ),
                ),
              ),
            ],
          ),
          AnimatedBuilder(
            animation: _animation,
            builder: (_, __) {
              return IgnorePointer(
                ignoring: _animation.value == 0,
                child: InkWell(
                  onTap: () {
                    _controller.reverse();
                  },
                  child: Container(
                    color:
                        Color(0xFFf4f1ea).withOpacity(_animation.value * 0.5),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
