import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_app/screens/otp_screen.dart';
import 'package:flutter_app_app/widgets/title_creating.dart';

import 'bottom_tabs.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  double _height, _width, _fixedPadding;

  @override
  void initState() {
    super.initState();
    getUser().then((user) {
      if (user != null) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => BottomTabs()));
        // send the user to the home page
        // homePage();
      }
    });
  }

  Future<FirebaseUser> getUser() async {
    return await _firebaseAuth.currentUser();
  }

  @override
  void dispose() {
    // While disposing the widget, we should close all the streams and controllers

    // Disposing Stream components
//    _countriesSink.close();
//    _countriesStreamController.close();

    // Disposing _countriesSearchController

    super.dispose();
  }

  static Country country = CountryPickerUtils.getCountryByIsoCode('UA');
  List<Country> countries = [];
  final TextEditingController _phoneNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _fixedPadding = _height * 0.025;
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.95),
      body: Center(
        child: SingleChildScrollView(
          child: Card(
            color: Color(0xFFf4f1ea),
            elevation: 2,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            child: SizedBox(
              height: _height * 8 / 10,
              width: _width * 8 / 10,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: _fixedPadding, left: _fixedPadding),
                    child: TextTitle('SELECT YOUR COUNTRY'),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: _fixedPadding, right: _fixedPadding),
                    child: Card(
                      child: InkWell(
                        onTap: _openCountryPickerDialog,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 4.0, right: 4.0, top: 8.0, bottom: 8.0),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 5),
                                child: CountryPickerUtils.getDefaultFlagImage(
                                    country),
                              ),
                              Text("${country.name}"),
                              Icon(Icons.arrow_drop_down, size: 24.0)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  TextTitle(
                    'MOBILE NUMBER',
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: _fixedPadding,
                        right: _fixedPadding,
                        bottom: _fixedPadding),
                    child: Card(
                      child: TextFormField(
                        keyboardType: TextInputType.phone,
                        controller: _phoneNumberController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          errorMaxLines: 1,
                          prefix: Text(" + " + country.phoneCode + "  "),
                        ),
                      ),
                    ),
                  ),
                  RaisedButton(
                    elevation: 15.0,
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => OTPScreen(
                              mobileNumber: '+' +
                                  country.phoneCode +
                                  _phoneNumberController.text,
                            ),
                          ));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'SEND OTP',
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                    ),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
          data: Theme.of(context).copyWith(primaryColor: Colors.pink),
          child: CountryPickerDialog(
            titlePadding: EdgeInsets.all(8.0),
            searchCursorColor: Colors.pinkAccent,
            searchInputDecoration: InputDecoration(hintText: 'Search...'),
            isSearchable: true,
            title: Text('Select your phone code'),
            onValuePicked: (Country pickedCountry) {
              setState(() {
                country = pickedCountry;
              });
            },
            itemBuilder: _buildDialogItem,
          ),
        ),
      );

  Widget _buildDialogItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );
}
