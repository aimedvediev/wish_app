import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app_app/screens/present_edit_screen.dart';
import 'package:provider/provider.dart';

import 'package:flutter_app_app/providers/presents_provider.dart';

class PresentDetailScreen extends StatefulWidget {
  static const routeName = '/present_detail';

  @override
  _PresentDetailScreenState createState() => _PresentDetailScreenState();
}

class _PresentDetailScreenState extends State<PresentDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final presentId = ModalRoute.of(context).settings.arguments as String;
    final loadedWishes = Provider.of<PresentProvider>(context, listen: false)
        .findById(presentId);
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color(0xFFfafafa),
            expandedHeight: 300,
            pinned: true,
            floating: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Column(
                children: <Widget>[
                  Image.file(
                    File(loadedWishes.image),
                  ),
                  FlatButton(
                    child: Text('EDIT'),
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                        PresentEditingScreen.routeName,
                        arguments: presentId,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
              child: Container(
            color: Color(0xFFf4f1ea),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 45, left: 50),
                  child: Text(
                    loadedWishes.name,
                    style: TextStyle(
                      color: Color(0xFF2f2f2f),
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 50),
                  child: Text(
                    loadedWishes.dateOfAdd,
                    style: TextStyle(
                      color: Color(0xFF1a1824),
                      fontSize: 12,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 25, left: 50),
                  child: Text(
                    'DESCRIPTION',
                    style: TextStyle(
                      color: Color(0xFF949391),
                      fontSize: 12,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 50, right: 30, top: 5),
                  child: Text(
                    loadedWishes.description,
                    style: TextStyle(
                      color: Color(0xFF2f2f2f),
                      fontSize: 18,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 50,
                    right: 250,
                    top: 60,
                  ),
                ),
                SizedBox(
                  height: 1000,
                ),
              ],
            ),
          )),
        ],
      ),
    );
  }
}
