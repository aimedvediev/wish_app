import 'package:flutter/material.dart';

import '../providers/contacts.dart';
import '../widgets/contact_item.dart';

class ContactsToInviteScreen extends StatefulWidget {
  static const routeName = '/contacts';

  @override
  _ContactsToInviteScreenState createState() => _ContactsToInviteScreenState();
}

class _ContactsToInviteScreenState extends State<ContactsToInviteScreen> {
  final List<Contacts> loadedContacts = [
    Contacts(
      id: '1',
      name: 'Anton',
      numbers: [
        '514264293',
        '514264294',
      ],
    ),
    Contacts(
      id: '2',
      name: 'Polina',
      numbers: [
        '123123123',
      ],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '3',
      name: 'Maks',
      numbers: ['321321321', '222333111', '333222111'],
    ),
    Contacts(
      id: '1',
      name: 'Anton',
      numbers: [
        '514264293',
        '514264294',
      ],
    ),
    Contacts(
      id: '1',
      name: 'Anton',
      numbers: [
        '514264293',
        '514264294',
      ],
    ),
    Contacts(
      id: '1',
      name: 'Anton',
      numbers: [
        '514264293',
        '514264294',
      ],
    ),
    Contacts(
      id: '1',
      name: 'Anton',
      numbers: [
        '514264293',
        '514264294',
      ],
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(left: 30, right: 15, top: 70, bottom: 20),
            color: Colors.white70,
            child: Text(
              'Invite people from your contacts',
              style: TextStyle(
                color: Color(0xFF222d6d),
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            height: 60,
            alignment: Alignment.center,
            padding: EdgeInsets.only(
              left: 30,
              right: 15,
            ),
            color: Colors.white70,
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search',
                hoverColor: Colors.grey,
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              color: Colors.white70,
              child: ListView.builder(
                itemCount: loadedContacts.length,
                itemBuilder: (_, index) => ContactItem(
                  loadedContacts[index].id,
                  loadedContacts[index].name,
                  loadedContacts[index].numbers,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
