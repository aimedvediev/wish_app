import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/auth.dart';
import 'package:flutter_app_app/widgets/text_notifier_home.dart';
import 'package:flutter_app_app/widgets/title_home.dart';
import 'package:provider/provider.dart';

import 'auth_screen.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
 String mobileTel = '';

  var _isInit = true;
  var _isLoading = false;

  void initState() {
    super.initState();
  }

  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Auth>(context).takeAuth().then((_) {
        setState(() {
          _isLoading = false;
          mobileTel=Provider.of<Auth>(context).mobileNumber;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(100),
                    ),
                    child: Container(
                      color: Color(0xFFf4f1ea),
                      height: 180,
                      width: 500, //todo
                      child: Padding(
                        padding: EdgeInsets.only(top: 60, left: 50),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                'Hello,',
                                style: TextStyle(
                                    color: Color.fromARGB(75, 0, 0, 0),
                                    fontSize: 20),
                              ),
                            ),
                            Container(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                '$mobileTel',
                                style: TextStyle(
                                  color: Color(0xFF454545),
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            RaisedButton(
                              child: Text("Sign out"),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8),
                                ),
                              ),
                              onPressed: () {
                                _firebaseAuth.signOut();
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AuthScreen()));
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                TitleHome('Don\'t forget'),
                TextNotifier(
                  'Mom\'s',
                  Color(0xFFf8b4c1),
                  'Birthday on Friday',
                ),
                TextNotifier(
                  'Lisa\'s',
                  Color(0xFF8dc8a5),
                  'party on Monday, 18th',
                ),
                TextNotifier(
                  '',
                  Color(0xFFf8b4c1),
                  'New Year in 55 days',
                ),
                TitleHome('TODAY'),
                TextNotifier(
                  'Tom Young',
                  Color(0xFF1a1824),
                  'started using the app',
                ),
                TextNotifier(
                  'Lisa',
                  Color(0xFF8dc8a5),
                  'added 3 ned presents to wishlist',
                ),
              ],
            ),
    );
  }
}
