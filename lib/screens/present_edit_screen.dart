import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/models/present.dart';
import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';
import 'package:provider/provider.dart';

import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;

import 'package:select_dialog/select_dialog.dart';

import 'package:flutter_app_app/widgets/chip_category.dart';
import 'package:flutter_app_app/widgets/title_creating.dart';

class PresentEditingScreen extends StatefulWidget {
  static const routeName = '/present_editing';

  @override
  _PresentEditingScreenState createState() => _PresentEditingScreenState();
}

class _PresentEditingScreenState extends State<PresentEditingScreen> {
  Color colorForFamily = Colors.black;

  static DateTime now = DateTime.now();
  String _formattedDate = DateFormat('HH:mm:ss').format(now);

  List<GROUPS> groupsList = [
    GROUPS.MYSELF,
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  final _form = GlobalKey<FormState>();

  GROUPS selectedGroup = GROUPS.MYSELF;

  Member member = Member(name: 'example');

  var _isInit = true;

  File _imageFile;
  String _filePath;
  StorageUploadTask _uploadTask;

  var _isLoading = false;

  var _editedPresent = Present(
    name: '',
    image: '',
    dateOfAdd: '',
    description: '',
    groups: GROUPS.MYSELF,
    idMember: '',
  );

  var _initValues = {
    'name': '',
    'dateofAdd': '',
    'groups': '',
    'idMember': '',
    'image': '',
    'description': '',
  };

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      Provider.of<PresentProvider>(context, listen: false)
          .updatePresent(_editedPresent);
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('An error occurred'),
          content: Text(error.toString()),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } finally {
      setState(() {
//        _startUpload();
        _isLoading = false;
      });
      Navigator.of(context).pop();
    }
  }

  final FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://flutter-wishapp.appspot.com');

  Future<void> _pickImage(ImageSource source) async {
    File selected = await ImagePicker.pickImage(source: source);

    final appDir = await syspath.getApplicationDocumentsDirectory();
    final fileName = path.basename(selected.path);
    final savedImage = await selected.copy('${appDir.path}/$fileName');

    setState(() {
      _filePath = '${DateTime.now()}${_editedPresent.idMember}.png';
      _imageFile = selected;

      _editedPresent = Present(
        name: _editedPresent.name,
        image: savedImage.path,
        dateOfAdd: _formattedDate,
        idMember: _editedPresent.idMember,
        description: _editedPresent.description,
        groups: _editedPresent.groups,
        id: _editedPresent.id,
      );
    });
  }

  Future<void> _startUpload() async {
    setState(() {
      _uploadTask = _storage
          .ref()
          .child('images/Presents/$_filePath')
          .putFile(_imageFile);
    });
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final presentId = ModalRoute.of(context).settings.arguments as String;
      _editedPresent = Provider.of<PresentProvider>(context, listen: false)
          .findById(presentId);
      _initValues = {
        'name': _editedPresent.name,
        'dateofAdd': _editedPresent.dateOfAdd,
        'idMember': _editedPresent.idMember,
        'groups': _editedPresent.groups.toString(),
        'image': _editedPresent.image,
        'description': _editedPresent.description,
        'id': _editedPresent.id,
      };
      selectedGroup = toChangeGroups(_editedPresent.groups.toString());
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final membersData = Provider.of<MembersProvider>(context);

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('HH:mm:ss').format(now);

    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _saveForm,
        label: Text(
          'SAVE',
          style: TextStyle(
            color: Colors.white70,
            fontSize: 16,
          ),
        ),
        backgroundColor: Color(0xFFfaae77),
      ),
      backgroundColor: Color(0xFFffffff),
      body: Padding(
        padding: EdgeInsets.only(
          left: 50,
          top: 70,
          right: 50,
        ),
        child: Form(
          key: _form,
          child: Container(
            child: ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    bottom: 40,
                  ),
                  child: Text(
                    'Present Creationg',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xFFfaae77),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    bottom: 40,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextTitle(
                        'NAME OF PRESENT',
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _initValues['name'],
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontSize: 16,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Enter present\'s name',
                          hasFloatingPlaceholder: false,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter present\'s name';
                          }
                          if (value.length < 3) {
                            return 'name must be at least 4';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _editedPresent = Present(
                            name: value,
                            image: _editedPresent.image,
                            dateOfAdd: _formattedDate,
                            idMember: _editedPresent.idMember,
                            description: _editedPresent.description,
                            groups: _editedPresent.groups,
                            id: _editedPresent.id,
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    bottom: 40,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextTitle(
                        'DESCRIPTION',
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _initValues['description'],
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontSize: 16,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Enter description',
                          hasFloatingPlaceholder: false,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter description';
                          }
                          if (value.length < 5) {
                            return 'description must be at least 6';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _editedPresent = Present(
                            name: _editedPresent.name,
                            image: _editedPresent.image,
                            dateOfAdd: _formattedDate,
                            idMember: _editedPresent.idMember,
                            description: value,
                            groups: _editedPresent.groups,
                            id: _editedPresent.id,
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(),
                  height: 70,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextTitle(
                        'CATEGORY',
                      ),
                      Container(
                        height: 50,
                        padding: EdgeInsets.only(top: 10),
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                ChipCategory(
                                  groups: groupsList,
                                  initialCategory: selectedGroup,
                                  onSelectionChip: (selected) {
                                    setState(
                                      () {
                                        selectedGroup = selected;
                                      },
                                    );
                                    if (selected != GROUPS.MYSELF)
                                      SelectDialog.showModal<Member>(context,
                                          label: "Chosse a Member",
                                          selectedValue: member,
                                          items: membersData
                                              .memberFromGroup(selectedGroup),
                                          itemBuilder: (BuildContext context,
                                              Member item, bool isSelected) {
                                        return Container(
                                          decoration: !isSelected
                                              ? null
                                              : BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  color: Colors.white,
                                                  border: Border.all(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                  ),
                                                ),
                                          child: ListTile(
                                            leading: CircleAvatar(
                                              backgroundImage:
                                                  Image.file(File(item.image))
                                                      .image,
                                            ),
                                            selected: isSelected,
                                            title: Text(item.name),
                                          ),
                                        );
                                      }, onChange: (selectedMember) {
                                        setState(() {
                                          member = selectedMember;
                                          _editedPresent = Present(
                                            name: _editedPresent.name,
                                            image: _editedPresent.image,
                                            dateOfAdd: _formattedDate,
                                            idMember: selectedMember.id,
                                            description:
                                                _editedPresent.description,
                                            groups: selectedGroup,
                                            id: _editedPresent.id,
                                          );
                                        });
                                      });
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 40, top: 40),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: TextTitle(
                          'PHOTO OR SCREENSHOT',
                        ),
                      ),
                      _initValues['image'] == null
                          ? Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8),
                                ),
                              ),
                              height: 50,
                              width: 50,
                              child: FlatButton(
                                child: Icon(
                                  Icons.add,
                                  size: 12,
                                  color: Colors.grey,
                                ),
                                onPressed: () =>
                                    _pickImage(ImageSource.gallery),
                              ),
                            )
                          : ClipRRect(
                              borderRadius: BorderRadius.all(
                                Radius.circular(6),
                              ),
                              child: FlatButton(
                                child: Image.file(
                                  File(_initValues['image']),
                                  height: 50,
                                  width: 50,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: () =>
                                    _pickImage(ImageSource.gallery),
                              ),
                            ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  GROUPS toChangeGroups(String groups) {
    return groups == 'GROUPS.FRIENDS'
        ? GROUPS.FRIENDS
        : groups == 'GROUPS.FAMILY'
            ? GROUPS.FAMILY
            : groups == 'GROUPS.MYSELF' ? GROUPS.MYSELF : GROUPS.COLLEAGUES;
  }
}
