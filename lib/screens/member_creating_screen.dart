import 'dart:io';

import 'package:country_pickers/country.dart';
import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;
import 'package:firebase_storage/firebase_storage.dart';

import 'package:flutter/material.dart';

import 'package:flutter_app_app/widgets/chip_category.dart';
import 'package:flutter_app_app/widgets/title_creating.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:provider/provider.dart';

class MemberCreatingScreen extends StatefulWidget {
  static const routeName = '/member_creating';

  @override
  _MemberCreatingScreenState createState() => _MemberCreatingScreenState();
}

class _MemberCreatingScreenState extends State<MemberCreatingScreen> {
  Color colorForFamily = Colors.black;

  File _imageFile;
  String _filePath;
  StorageUploadTask _uploadTask;

  var _isLoading = false;

  List<GROUPS> groupsList = [
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup;

  DateTime _dateTime;
  static Country country = CountryPickerUtils.getCountryByIsoCode('PL');

  final _form = GlobalKey<FormState>();
  var _editedMember = Member(
    name: '',
    phoneNumber: '123',
    dateOfBD: '1212',
    groups: GROUPS.FAMILY,
    image:
        'https://cdn.cnn.com/cnnnext/dam/assets/191024091949-02-foster-cat-large-169.jpg',
  );

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (_imageFile == null || selectedGroup == null || _dateTime == null) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error'),
              content: Text('Please select photo, category, birthday date'),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
      return;
    }
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      Provider.of<MembersProvider>(context, listen: false)
          .addMember(_editedMember);
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('An error occurred'),
          content: Text(error.toString()),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } finally {
      setState(() {
//        _startUpload();
//        StorageReference ref = FirebaseStorage.instance
//            .ref()
//            .child('images/Members/$_filePath}');
        _isLoading = false;
      });
      Navigator.of(context).pop();
    }
  }

  final FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://flutter-wishapp.appspot.com');

  Future<void> _pickImage(ImageSource source) async {
    File selected = await ImagePicker.pickImage(source: source, maxWidth: 600);

    final appDir = await syspath.getApplicationDocumentsDirectory();
    final fileName = path.basename(selected.path);
    final savedImage = await selected.copy('${appDir.path}/$fileName');

    setState(() {
      _filePath = '${DateTime.now()}${_editedMember.phoneNumber}.png';
      _imageFile = selected;

      _editedMember = Member(
        name: _editedMember.name,
        phoneNumber: _editedMember.phoneNumber,
        dateOfBD: _editedMember.dateOfBD,
        groups: _editedMember.groups,
        image: savedImage.path,
      );
    });
  }

  Future<void> _startUpload() async {
    setState(() {
      _uploadTask =
          _storage.ref().child('images/Members/$_filePath').putFile(_imageFile);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _saveForm,
        label: Text(
          'SAVE',
          style: TextStyle(
            color: Color(0xFF222d6d),
            fontSize: 16,
          ),
        ),
        backgroundColor: Color(0xFFf4f1ea),
      ),
      backgroundColor: Color(0xFFffffff),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.only(
                left: 50,
                top: 70,
                right: 50,
              ),
              child: Form(
                key: _form,
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Text(
                        'Create person you need',
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF222d6d),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'MOBILE NUMBER',
                          ),
                          Row(
                            children: <Widget>[
                              FlatButton(
                                padding: EdgeInsets.only(top: 15),
                                child: Row(
                                  children: <Widget>[
                                    CountryPickerUtils.getDefaultFlagImage(
                                        country),
                                    SizedBox(
                                      width: 8.0,
                                    ),
                                    Text("+${country.phoneCode}"),
                                  ],
                                ),
                                onPressed: _openCountryPickerDialog,
                              ),
                              Expanded(
                                child: TextFormField(
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(
                                    color: Color(0xFF000000),
                                    fontSize: 16,
                                  ),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    labelText: 'Enter phone number',
                                    hasFloatingPlaceholder: false,
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please provide a number';
                                    }
                                    if (value.length != 9) {
                                      return 'Incorect length of number';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    _editedMember = Member(
                                      name: _editedMember.name,
                                      phoneNumber: value,
                                      dateOfBD: _editedMember.dateOfBD,
                                      groups: _editedMember.groups,
                                      image: _editedMember.image,
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'Name',
                          ),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            style: TextStyle(
                              color: Color(0xFF000000),
                              fontSize: 16,
                            ),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              labelText: 'Enter person\'s name',
                              hasFloatingPlaceholder: false,
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please provide a name';
                              }
                              if (value.length < 1) {
                                return 'Must be at least 2';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              _editedMember = Member(
                                name: value,
                                phoneNumber: '+${country.phoneCode}' +
                                    _editedMember.phoneNumber,
                                dateOfBD: _editedMember.dateOfBD,
                                groups: _editedMember.groups,
                                image: _editedMember.image,
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'BIRTHDAY DATE',
                          ),
                          DatePickerWidget(
                            dateFormat: 'dd|MMMM|yyyy',
                            initialDateTime: DateTime.parse('2019-05-17'),
                            onChange: (dateTime, selectedIndex) {
                              String month;
                              setState(() {
                                _dateTime = dateTime;
                                if (dateTime.month < 10) {
                                  month = '0${dateTime.month}';
                                }
                                _editedMember = Member(
                                  name: _editedMember.name,
                                  phoneNumber: '+${country.phoneCode}' +
                                      _editedMember.phoneNumber,
                                  dateOfBD:
                                      '${_dateTime.year}-$month-${_dateTime.day}',
                                  groups: _editedMember.groups,
                                  image: _editedMember.image,
                                );
                              });
                            },
                            pickerTheme: DateTimePickerTheme(
                              showTitle: false,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        bottom: 40,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: TextTitle(
                              'PHOTO',
                            ),
                          ),
                          _imageFile == null
                              ? Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 0.5,
                                      color: Colors.grey,
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                  ),
                                  height: 50,
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.add,
                                      size: 12,
                                      color: Colors.grey,
                                    ),
                                    onPressed: () =>
                                        _pickImage(ImageSource.gallery),
                                  ),
                                )
                              : ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(6),
                                  ),
                                  child: FlatButton(
                                    child: Image.file(
                                      _imageFile,
                                      height: 50,
                                      width: 50,
                                      fit: BoxFit.cover,
                                    ),
                                    onPressed: () =>
                                        _pickImage(ImageSource.gallery),
                                  ),
                                ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(),
                      height: 70,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          TextTitle(
                            'CATEGORY',
                          ),
                          Container(
                            height: 50,
                            padding: EdgeInsets.only(top: 10),
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    ChipCategory(
                                      initialCategory: _editedMember.groups,
                                      groups: groupsList,
                                      onSelectionChip: (selected) {
                                        setState(
                                          () {
                                            selectedGroup = selected;
                                            _editedMember = Member(
                                              name: _editedMember.name,
                                              phoneNumber:
                                                  '+${country.phoneCode}' +
                                                      _editedMember.phoneNumber,
                                              dateOfBD: _editedMember.dateOfBD,
                                              groups: selected,
                                              image: _editedMember.image,
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
          data: Theme.of(context).copyWith(primaryColor: Colors.pink),
          child: CountryPickerDialog(
            titlePadding: EdgeInsets.all(8.0),
            searchCursorColor: Colors.pinkAccent,
            searchInputDecoration: InputDecoration(hintText: 'Search...'),
            isSearchable: true,
            title: Text('Select your phone code'),
            onValuePicked: (Country pickedCountry) {
              setState(() {
                country = pickedCountry;
                _editedMember = Member(
                  name: _editedMember.name,
                  phoneNumber:
                      '+${pickedCountry.phoneCode}' + _editedMember.phoneNumber,
                  dateOfBD: _editedMember.dateOfBD,
                  groups: GROUPS.COLLEAGUES,
                  image: _editedMember.image,
                );
              });
            },
            itemBuilder: _buildDialogItem,
          ),
        ),
      );

  Widget _buildDialogItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );
}
