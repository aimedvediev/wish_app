import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';

import 'package:flutter_app_app/screens/present_creating_screen.dart';

import 'package:flutter_app_app/widgets/chips_filter.dart';
import 'package:flutter_app_app/widgets/presents_listview.dart';
import 'package:provider/provider.dart';

class ListOfPresentScreen extends StatefulWidget {
  static const routeName = '/presents_list';

  @override
  _ListOfPresentScreenState createState() => _ListOfPresentScreenState();
}

class _ListOfPresentScreenState extends State<ListOfPresentScreen> {
  List<GROUPS> groupsList = [
    GROUPS.ALL,
    GROUPS.MYSELF,
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  GROUPS selectedGroup = GROUPS.ALL;

  var _isInit = true;
  var _isLoading = false;

  void initState() {
    super.initState();
  }

  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<PresentProvider>(context).fetchAndSetPresents().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed(PresentCreatingScreen.routeName);
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xFF454545),
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(
              top: 80,
              left: 45,
              right: 15,
            ),
            child: Text(
              "List of presents",
              style: TextStyle(
                color: Color(0xFF454545),
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 15, bottom: 5, left: 25),
            height: 60,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                ChipsFilter(
                  groups: groupsList,
                  onSelectionChip: (selected) {
                    setState(
                      () {
                        selectedGroup = selected;
                      },
                    );
                  },
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 45, top: 20, bottom: 12),
            child: Text(
              'ON LIST(5)',
              style: TextStyle(
                fontSize: 10,
                color: Color(0xFFb4b4b4),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(),
              child: _isLoading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : PresentsListView(
                      groups: selectedGroup,
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
