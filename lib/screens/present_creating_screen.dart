import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/models/present.dart';
import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';
import 'package:provider/provider.dart';

import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;

import 'package:select_dialog/select_dialog.dart';

import 'package:flutter_app_app/widgets/chip_category.dart';
import 'package:flutter_app_app/widgets/title_creating.dart';

class PresentCreatingScreen extends StatefulWidget {
  static const routeName = '/present_creating';

  @override
  _PresentCreatingScreenState createState() => _PresentCreatingScreenState();
}

class _PresentCreatingScreenState extends State<PresentCreatingScreen> {
  Color colorForFamily = Colors.black;

  List<GROUPS> groupsList = [
    GROUPS.MYSELF,
    GROUPS.FAMILY,
    GROUPS.FRIENDS,
    GROUPS.COLLEAGUES,
  ];

  final _form = GlobalKey<FormState>();

  GROUPS selectedGroup;

  Member member = Member(name: 'example');

  File _imageFile;
  String _filePath;
  StorageUploadTask _uploadTask;

  var _isLoading = false;

  var _editedPresent = Present(
    name: '',
    image: '',
    dateOfAdd: '',
    idMember: '',
    description: '',
    groups: GROUPS.MYSELF,
  );

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (_imageFile == null || selectedGroup == null) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error'),
              content: Text('Please select photo, category'),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
      return;
    }
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      if (selectedGroup == GROUPS.MYSELF) {
        await Provider.of<PresentProvider>(context, listen: false)
            .addMyPresent(_editedPresent);
      } else {
        Provider.of<PresentProvider>(context, listen: false)
            .addPresent(_editedPresent);
      }
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('An error occurred'),
          content: Text(error.toString()),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } finally {
      setState(() {
//        _startUpload();
        _isLoading = false;
      });
      Navigator.of(context).pop();
    }
  }

  final FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://flutter-wishapp.appspot.com');

  Future<void> _pickImage(ImageSource source) async {
    File selected = await ImagePicker.pickImage(source: source);
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('HH:mm:ss').format(now);

    final appDir = await syspath.getApplicationDocumentsDirectory();
    final fileName = path.basename(selected.path);
    final savedImage = await selected.copy('${appDir.path}/$fileName');

    setState(() {
      _filePath = '${DateTime.now()}${_editedPresent.idMember}.png';
      _imageFile = selected;

      _editedPresent = Present(
        name: _editedPresent.name,
        image: savedImage.path,
        dateOfAdd: formattedDate,
        idMember: _editedPresent.idMember,
        description: _editedPresent.description,
        groups: _editedPresent.groups,
      );
    });
  }

  Future<void> _startUpload() async {
    setState(() {
      _uploadTask = _storage
          .ref()
          .child('images/Presents/$_filePath')
          .putFile(_imageFile);
    });
  }

  @override
  Widget build(BuildContext context) {
    final membersData = Provider.of<MembersProvider>(context);

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('HH:mm:ss').format(now);

    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _saveForm,
        label: Text(
          'SAVE',
          style: TextStyle(
            color: Colors.white70,
            fontSize: 16,
          ),
        ),
        backgroundColor: Color(0xFFfaae77),
      ),
      backgroundColor: Color(0xFFffffff),
      body: Padding(
        padding: EdgeInsets.only(
          left: 50,
          top: 70,
          right: 50,
        ),
        child: Form(
          key: _form,
          child: Container(
            child: ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    bottom: 40,
                  ),
                  child: Text(
                    'Present Creationg',
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xFFfaae77),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    bottom: 40,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextTitle(
                        'NAME OF PRESENT',
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontSize: 16,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Enter present\'s name',
                          hasFloatingPlaceholder: false,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter present\'s name';
                          }
                          if (value.length < 3) {
                            return 'name must be at least 4';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _editedPresent = Present(
                            name: value,
                            image: _editedPresent.image,
                            dateOfAdd: _editedPresent.dateOfAdd,
                            idMember: _editedPresent.idMember,
                            description: _editedPresent.description,
                            groups: _editedPresent.groups,
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    bottom: 40,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextTitle(
                        'DESCRIPTION',
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                          color: Color(0xFF000000),
                          fontSize: 16,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          labelText: 'Enter description',
                          hasFloatingPlaceholder: false,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter description';
                          }
                          if (value.length < 5) {
                            return 'description must be at least 6';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _editedPresent = Present(
                            name: _editedPresent.name,
                            image: _editedPresent.image,
                            dateOfAdd: _editedPresent.dateOfAdd,
                            idMember: _editedPresent.idMember,
                            description: value,
                            groups: _editedPresent.groups,
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(),
                  height: 70,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextTitle(
                        'CATEGORY',
                      ),
                      Container(
                        height: 50,
                        padding: EdgeInsets.only(top: 10),
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                ChipCategory(
                                  groups: groupsList,
                                  onSelectionChip: (selected) {
                                    setState(
                                      () {
                                        selectedGroup = selected;
                                      },
                                    );
                                    if (selected != GROUPS.MYSELF)
                                      SelectDialog.showModal<Member>(context,
                                          label: "Chosse a Member",
                                          selectedValue: member,
                                          items: membersData
                                              .memberFromGroup(selectedGroup),
                                          itemBuilder: (BuildContext context,
                                              Member item, bool isSelected) {
                                        return Container(
                                          decoration: !isSelected
                                              ? null
                                              : BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  color: Colors.white,
                                                  border: Border.all(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                  ),
                                                ),
                                          child: ListTile(
                                            leading: CircleAvatar(
                                              backgroundImage:
                                                  Image.file(File(item.image))
                                                      .image,
                                            ),
                                            selected: isSelected,
                                            title: Text(item.name),
                                          ),
                                        );
                                      }, onChange: (selectedMember) {
                                        setState(() {
                                          member = selectedMember;
                                          _editedPresent = Present(
                                            name: _editedPresent.name,
                                            image: _editedPresent.image,
                                            dateOfAdd: formattedDate,
                                            idMember: selectedMember.phoneNumber,
                                            description:
                                                _editedPresent.description,
                                            groups: selectedGroup,
                                          );
                                        });
                                      });
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 40, top: 40),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: TextTitle(
                          'PHOTO OR SCREENSHOT',
                        ),
                      ),
                      _imageFile == null
                          ? Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8),
                                ),
                              ),
                              height: 50,
                              width: 50,
                              child: FlatButton(
                                child: Icon(
                                  Icons.add,
                                  size: 12,
                                  color: Colors.grey,
                                ),
                                onPressed: () =>
                                    _pickImage(ImageSource.gallery),
                              ),
                            )
                          : ClipRRect(
                              borderRadius: BorderRadius.all(
                                Radius.circular(6),
                              ),
                              child: FlatButton(
                                child: Image.file(
                                  _imageFile,
                                  height: 50,
                                  width: 50,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: () =>
                                    _pickImage(ImageSource.gallery),
                              ),
                            ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
