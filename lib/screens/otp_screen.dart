import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/auth.dart';
import 'package:flutter_app_app/widgets/otp_input.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'bottom_tabs.dart';

class OTPScreen extends StatefulWidget {
  final String mobileNumber;

  OTPScreen({
    Key key,
    @required this.mobileNumber,
  })  : assert(mobileNumber != null),
        super(key: key);

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  /// Control the input text field.
  TextEditingController _pinEditingController = TextEditingController();

  /// Decorate the outside of the Pin.
  PinDecoration _pinDecoration =
      UnderlineDecoration(enteredColor: Colors.black, hintText: '123456');

  bool isCodeSent = false;
  String _verificationId;

  double _height, _width, _fixedPadding;

  FocusNode focusNode1 = FocusNode();
  FocusNode focusNode2 = FocusNode();
  FocusNode focusNode3 = FocusNode();
  FocusNode focusNode4 = FocusNode();
  FocusNode focusNode5 = FocusNode();
  FocusNode focusNode6 = FocusNode();
  String code = "";

  @override
  void initState() {
    super.initState();
    _onVerifyCode();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _fixedPadding = _height * 0.025;

    print("isValid - $isCodeSent");
    print("mobiel ${widget.mobileNumber}");
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.95),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Card(
              color: Color(0xFFf4f1ea),
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              child: SizedBox(
                height: _height * 8 / 10,
                width: _width * 8 / 10,
                child: Column(
                  children: <Widget>[
                    //  Logo: scaling to occupy 2 parts of 10 in the whole height of device
                    Row(
                      children: <Widget>[
                        SizedBox(width: 16.0),
                        Expanded(
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: 'Please enter the ',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400)),
                                TextSpan(
                                    text: 'One Time Password',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w700)),
                                TextSpan(
                                  text: ' sent to your mobile',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: 16.0),
                      ],
                    ),

                    SizedBox(height: 16.0),

                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        getPinField(key: "1", focusNode: focusNode1),
                        SizedBox(width: 5.0),
                        getPinField(key: "2", focusNode: focusNode2),
                        SizedBox(width: 5.0),
                        getPinField(key: "3", focusNode: focusNode3),
                        SizedBox(width: 5.0),
                        getPinField(key: "4", focusNode: focusNode4),
                        SizedBox(width: 5.0),
                        getPinField(key: "5", focusNode: focusNode5),
                        SizedBox(width: 5.0),
                        getPinField(key: "6", focusNode: focusNode6),
                        SizedBox(width: 5.0),
                      ],
                    ),

                    SizedBox(height: 32.0),
                    RaisedButton(
                      elevation: 16.0,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'VERIFY',
                          style: TextStyle(
                              color: Colors.black, fontSize: 18.0),
                        ),
                      ),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      onPressed: () {
                        if (code.length==6) {
                          _onFormSubmitted();
                        } else {
                          showToast("Invalid OTP", Colors.black);
                        }
                      },
                    )
                  ],

//    child: Column(
//              children: <Widget>[
//                Padding(
//                  padding: const EdgeInsets.all(16.0),
//                  child: PinInputTextField(
//                    pinLength: 6,
//                    decoration: _pinDecoration,
//                    controller: _pinEditingController,
//                    autoFocus: true,
//                    textInputAction: TextInputAction.done,
//                    onSubmit: (pin) {
//                      if (pin.length == 6) {
//                        _onFormSubmitted();
//                      } else {
//                        showToast("Invalid OTP", Colors.red);
//                      }
//                    },
//                  ),
//                ),
//                Container(
//                  padding: EdgeInsets.all(16),
//                  child: Center(
//                    child: SizedBox(
//                      width: MediaQuery.of(context).size.width * 0.8,
//                      child: RaisedButton(
//                        color: Theme.of(context).primaryColor,
//                        shape: RoundedRectangleBorder(
//                            borderRadius: BorderRadius.circular(0.0)),
//                        child: Text(
//                          "ENTER OTP",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 18.0,
//                              fontWeight: FontWeight.bold),
//                        ),
//                        onPressed: () {
//                          if (_pinEditingController.text.length == 6) {
//                            _onFormSubmitted();
//                          } else {
//                            showToast("Invalid OTP", Colors.red);
//                          }
//                        },
//                        padding: EdgeInsets.all(16.0),
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getPinField({String key, FocusNode focusNode}) => SizedBox(
        height: 40.0,
        width: 35.0,
        child: TextField(
          key: Key(key),
          expands: false,
          autofocus: key.contains("1") ? true : false,
          focusNode: focusNode,
          onChanged: (String value) {
            if (value.length == 1) {
              code += value;
              switch (code.length) {
                case 1:
                  FocusScope.of(context).requestFocus(focusNode2);
                  break;
                case 2:
                  FocusScope.of(context).requestFocus(focusNode3);
                  break;
                case 3:
                  FocusScope.of(context).requestFocus(focusNode4);
                  break;
                case 4:
                  FocusScope.of(context).requestFocus(focusNode5);
                  break;
                case 5:
                  FocusScope.of(context).requestFocus(focusNode6);
                  break;
                default:
                  FocusScope.of(context).requestFocus(FocusNode());
                  break;
              }
            }
          },
          maxLengthEnforced: false,
          textAlign: TextAlign.center,
          cursorColor: Colors.black,
          keyboardType: TextInputType.number,
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      );

  void showToast(message, Color color) {
    print(message);
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 2,
        backgroundColor: color,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  void _onVerifyCode() async {
    setState(() {
      isCodeSent = true;
    });
    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential phoneAuthCredential) {
      _firebaseAuth
          .signInWithCredential(phoneAuthCredential)
          .then((AuthResult value) {
        if (value.user != null) {
          // Handle loogged in state
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => BottomTabs(),
              ),
              (Route<dynamic> route) => false);
        } else {
          showToast("Error validating OTP, try again", Colors.red);
        }
      }).catchError((error) {
        showToast("Try again in sometime", Colors.red);
      });
    };
    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      showToast(authException.message, Colors.red);
      setState(() {
        isCodeSent = false;
      });
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      _verificationId = verificationId;
      setState(() {
        _verificationId = verificationId;
      });
    };
    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
      setState(() {
        _verificationId = verificationId;
      });
    };

    await _firebaseAuth.verifyPhoneNumber(
        phoneNumber: "${widget.mobileNumber}",
        timeout: const Duration(seconds: 60),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  void _onFormSubmitted() async {
    AuthCredential _authCredential = PhoneAuthProvider.getCredential(
        verificationId: _verificationId, smsCode: code);

    _firebaseAuth
        .signInWithCredential(_authCredential)
        .then((AuthResult value) {
      if (value.user != null) {
        // Handle loogged in state

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => BottomTabs(),
            ),
            (Route<dynamic> route) => false);
      } else {
        showToast("Error validating OTP, try again", Colors.red);
      }
    }).catchError((error) {
      showToast("Something went wrong", Colors.red);
    });
  }
}
