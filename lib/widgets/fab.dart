import 'package:flutter/material.dart';

class FabItem {
  const FabItem(this.title, this.color, {this.onPress});

  final Function onPress;
  final String title;
  final Color color;
}

class FabMenuItem extends StatelessWidget {
  const FabMenuItem(this.item, {Key key}) : super(key: key);

  final FabItem item;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10),),
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 24, right: 16),
      color: item.color,
      splashColor: Colors.grey.withOpacity(0.1),
      highlightColor: Colors.grey.withOpacity(0.1),
      disabledColor: Colors.white,
      onPressed: item.onPress,
      child: Container(
        child: Text(
          item.title,
          style: TextStyle(
            color: Color(0xFFf4f1ea),
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}

class ExpandedAnimationFab extends AnimatedWidget {
  const ExpandedAnimationFab({
    @required this.items,
    this.onPress,
    Animation animation,
  }) : super(listenable: animation);

  final List<FabItem> items;
  final Function onPress;

  get _animation => listenable;

  Widget buildItem(BuildContext context, int index) {
    final screenWidth = MediaQuery.of(context).size.width;

    final transform = Matrix4.translationValues(
      -(screenWidth - _animation.value * screenWidth) *
          ((items.length - index) / 4),
      0.0,
      0.0,
    );

    return Align(
      alignment: Alignment.centerRight,
      child: Transform(
        transform: transform,
        child: Opacity(
          opacity: _animation.value,
          child: FabMenuItem(items[index]),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        IgnorePointer(
          ignoring: _animation.value == 0,
          child: ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            separatorBuilder: (_, __) => SizedBox(height: 9),
            padding: EdgeInsets.symmetric(vertical: 12),
            itemCount: items.length,
            itemBuilder: buildItem,
          ),
        ),
        FloatingActionButton(
          backgroundColor: Color(0xFF454545),
          child: AnimatedIcon(
            icon: AnimatedIcons.menu_close,
            progress: _animation,
          ),
          onPressed: onPress,
        ),
      ],
    );
  }
}
