import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_app_app/models/member.dart';

class MemberItem extends StatelessWidget {
  final String name;
  final String dateOfBD;
  final String image;
  final GROUPS groups;

  MemberItem(
    this.name,
    this.dateOfBD,
    this.image,
    this.groups,
  );

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        name,
        style: TextStyle(
          color: Color(0xFF2f2f2f),
          fontWeight: FontWeight.bold,
          fontSize: 14,
        ),
      ),
      subtitle: Text(
        dateOfBD,
        style: TextStyle(
          color: Color(0xFF1a1824),
          fontSize: 12,
        ),
      ),
      leading: ClipRRect(
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
                left: BorderSide(
              color: groups == GROUPS.FAMILY
                  ? Color(0xFFf8b4c1)
                  : groups == GROUPS.FRIENDS
                      ? Color(0xFF8dc8a5)
                      : Color(0xFF91bedc),
              width: 6,
            )),
          ),
          child: Image.file(
            File(image),
            fit: BoxFit.cover,
            height: 52,
            width: 46,
          ),
        ),
      ),
    );
  }
}
