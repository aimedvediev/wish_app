import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:provider/provider.dart';

import 'package:flutter_app_app/screens/present_detail_screen.dart';
import 'package:flutter_app_app/widgets/present_item.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';

class PresentsListView extends StatelessWidget {
  GROUPS groups;

  PresentsListView({
    this.groups,
  });

  @override
  Widget build(BuildContext context) {
    final wishesData = Provider.of<PresentProvider>(context);
    final wish = wishesData.wishesFromGroup(groups);

    return ListView.builder(
      itemCount: wish.length,
      itemBuilder: (_, index) => FlatButton(
        child: WishItem(
          wish[index].id,
          wish[index].name,
          wish[index].idMember,
          wish[index].dateOfAdd,
          wish[index].image,
          wish[index].groups,
          isYouCanDelete: true,
        ),
        onPressed: () {
          Navigator.of(context).pushNamed(
            PresentDetailScreen.routeName,
            arguments: wish[index].id,
          );
        },
      ),
    );
  }
}
