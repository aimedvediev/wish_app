import 'package:flutter/material.dart';

class TextFieldCreating extends StatelessWidget {
  final TextInputType textInputType;
  final String labelText;

  const TextFieldCreating(
    this.textInputType,
    this.labelText,
  );

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: textInputType,
      style: TextStyle(
        color: Color(0xFF000000),
        fontSize: 16,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        labelText: labelText,
        hasFloatingPlaceholder: false,
      ),
    );
  }
}


