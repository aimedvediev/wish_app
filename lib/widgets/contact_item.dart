import 'package:flutter/material.dart';

class ContactItem extends StatelessWidget {
  final String id;
  final String name;
  final List<String> phones;

  ContactItem(
    this.id,
    this.name,
    this.phones,
  );

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: ListTile(
            title: Text(
              name,
              style: TextStyle(
                fontSize: 14,
                color: Color(0xFF222d6d),
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              phones[0],
              style: TextStyle(
                fontSize: 12,
                color: Color(0xFF787993),
              ),
            ),
          ),
        ),
        Container(
          height: 28,
          width: 78,
          child: RaisedButton(
            splashColor: Colors.blueAccent,
            color: Color(0xFF222d6d),
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(
                color: Color(0xFF222d6d),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 14,
                ),
                Text(
                  'Invite',
                  style: TextStyle(color: Colors.white, fontSize: 12),
                ),
              ],
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }
}
