import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:provider/provider.dart';

import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_app_app/screens/member_detail_screen.dart';

import '../widgets/members_items.dart';

class MembersListView extends StatelessWidget {
  GROUPS groups;

  MembersListView({
    this.groups,
  });

  @override
  Widget build(BuildContext context) {
    final membersData = Provider.of<MembersProvider>(context);
    final members = membersData.memberFromGroup(groups);

    return ListView.builder(
      itemCount: members.length,
      itemBuilder: (_, index) => FlatButton(
        child: MemberItem(
          members[index].name,
          members[index].dateOfBD,
          members[index].image,
          members[index].groups,
        ),
        onPressed: () {
          Navigator.of(context).pushNamed(
            MemberDetailScreen.routeName,
            arguments:
                members[index].id,

          );
        },
      ),
    );
  }
}
