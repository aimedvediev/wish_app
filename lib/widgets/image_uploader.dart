import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class Uploader extends StatefulWidget {
  final File file;
  final String phoneNumber;

  Uploader({
    this.file,
    this.phoneNumber,
  });

  createState() => _UploaderState();
}

class _UploaderState extends State<Uploader> {
  final FirebaseStorage _storage =
      FirebaseStorage(storageBucket: 'gs://flutter-wishapp.appspot.com');

  StorageUploadTask _uploadTask;

  void _startUpload() {
    String filePath =
        'images/Members/${DateTime.now()}${widget.phoneNumber}.png';

    setState(() {
      _uploadTask = _storage.ref().child(filePath).putFile(widget.file);
    });
  }

  Widget build(BuildContext context) {}
}
