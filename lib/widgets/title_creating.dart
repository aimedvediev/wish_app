import 'package:flutter/material.dart';

class TextTitle extends StatelessWidget {
  final String title;

  const TextTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color(0xFF000000),
        fontSize: 10,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
