import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';
import 'package:flutter_app_app/providers/members_provider.dart';
import 'package:flutter_app_app/providers/presents_provider.dart';
import 'package:provider/provider.dart';

class WishItem extends StatelessWidget {
  final String id;
  final String name;
  final String forWhomWish;
  final String dateOfAdding;
  final String imageUrl;
  final GROUPS groups;
  final bool isYouCanDelete;

  const WishItem(
    this.id,
    this.name,
    this.forWhomWish,
    this.dateOfAdding,
    this.imageUrl,
    this.groups, {
    this.isYouCanDelete,
  });

  @override
  Widget build(BuildContext context) {
    final loadedMember = Provider.of<MembersProvider>(context, listen: false)
        .findByPhoneCountryNumber(forWhomWish);
    return Padding(
      padding: const EdgeInsets.only(right: 0),
      child: isYouCanDelete
          ? Container(
              height: 88,
              width: 350,
              child: Dismissible(
                direction: DismissDirection.endToStart,
                key: ValueKey(id),
                onDismissed: (direction) {
                  Provider.of<PresentProvider>(context, listen: false)
                      .deletePresent(id,forWhomWish);
                },
                background: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: 20),
                  color: Colors.red,
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                    size: 40,
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Row(
                        children: <Widget>[
                          Text(name),
                          forWhomWish != ''
                              ? Text(
                                  ' for ' + loadedMember.name,
                                  style: TextStyle(
                                    color: groups == GROUPS.FAMILY
                                        ? Color(0xFFf8b4c1)
                                        : groups == GROUPS.FRIENDS
                                            ? Color(0xFF8dc8a5)
                                            : groups == GROUPS.COLLEAGUES
                                                ? Color(0xFF91bedc)
                                                : Color(0xFFfaae77),
                                  ),
                                )
                              : Text(''),
                        ],
                      ),
                      subtitle: Text(dateOfAdding),
                      leading: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(6),
                        ),
                        child: Container(
                          child: Image.file(
                            File(imageUrl),
                            height: 52,
                            width: 52,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 75),
                      child: Divider(),
                    ),
                  ],
                ),
              ),
            )
          : Container(
              height: 88,
              width: 350,
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Row(
                      children: <Widget>[
                        Text(name),
                        forWhomWish != ''
                            ? Text(
                                ' for ' + loadedMember.name,
                                style: TextStyle(
                                  color: groups == GROUPS.FAMILY
                                      ? Color(0xFFf8b4c1)
                                      : groups == GROUPS.FRIENDS
                                          ? Color(0xFF8dc8a5)
                                          : groups == GROUPS.COLLEAGUES
                                              ? Color(0xFF91bedc)
                                              : Color(0xFFfaae77),
                                ),
                              )
                            : Text(''),
                      ],
                    ),
                    subtitle: Text(dateOfAdding),
                    leading: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(6),
                      ),
                      child: Container(
                        child: Image.network(
                          imageUrl,
                          height: 52,
                          width: 52,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 75),
                    child: Divider(),
                  ),
                ],
              ),
            ),
    );
  }
}
