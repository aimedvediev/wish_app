import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';

class ChipsFilter extends StatefulWidget {
  final List<GROUPS> groups;
  final Function(GROUPS) onSelectionChip;

  ChipsFilter({
    this.groups,
    this.onSelectionChip,
  });

  @override
  _ChipsFilterState createState() => _ChipsFilterState();
}

class _ChipsFilterState extends State<ChipsFilter> {
  GROUPS selectedGroup = GROUPS.ALL;

  _buildChoiceList() {
    List<Widget> choices = List();

    widget.groups.forEach((item) {
      choices.add(Container(
        padding: EdgeInsets.only(left: 20),
        child: ChoiceChip(
          label: item == GROUPS.ALL
              ? Text('All')
              : item == GROUPS.FAMILY
                  ? Text('family')
                  : item == GROUPS.FRIENDS
                      ? Text('friends')
                      : item == GROUPS.COLLEAGUES
                          ? Text('colleagues')
                          : Text('mine'),
          labelStyle: TextStyle(
            color: item == GROUPS.ALL ? Colors.black : Colors.white70,
            fontSize: 14,
            fontWeight: item == GROUPS.ALL && selectedGroup == GROUPS.ALL
                ? FontWeight.bold
                : FontWeight.normal,
          ),
          selected: selectedGroup == item,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
          ),
          backgroundColor: item == GROUPS.ALL
              ? Color(0xFFf4f1ea)
              : item == GROUPS.FAMILY
                  ? Color.fromRGBO(248, 180, 193, 0.4)
                  : item == GROUPS.FRIENDS
                      ? Color.fromRGBO(141, 200, 165, 0.4)
                      : item == GROUPS.COLLEAGUES
                          ? Color.fromRGBO(145, 190, 220, 0.4)
                          : Color(0xFFf7d0b4),
          selectedColor: item == GROUPS.ALL
              ? Color(0xFFf4f1ea)
              : item == GROUPS.FAMILY
                  ? Color(0xFFf8b4c1)
                  : item == GROUPS.FRIENDS
                      ? Color(0xFF8dc8a5)
                      : item == GROUPS.COLLEAGUES
                          ? Color(0xFF91bedc)
                          : Color(0xFFfaae77),
          selectedShadowColor: item == GROUPS.ALL
              ? Color(0xFFf4f1ea)
              : item == GROUPS.FAMILY
                  ? Color(0xFFf8b4c1)
                  : item == GROUPS.FRIENDS
                      ? Color(0xFF8dc8a5)
                      : item == GROUPS.COLLEAGUES
                          ? Color(0xFF91bedc)
                          : Color(0xFFfaae77),
          onSelected: (isSelected) {
            setState(() {
              selectedGroup = item;
              widget.onSelectionChip(selectedGroup);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
