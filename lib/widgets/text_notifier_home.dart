import 'package:flutter/material.dart';

class TextNotifier extends StatelessWidget {
  final String name;
  final Color color;
  final String text;

  const TextNotifier(
    this.name,
    this.color,
    this.text,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 75,
        top: 10,
      ),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              Icons.brightness_1,
              size: 4,
              color: Color(0xFFb4b4b4),
            ),
          ),
          Text(
            name+' ',
            style: TextStyle(
              color: color,
              fontSize: 16,
              fontWeight: FontWeight.bold
            ),
          ),
          Text(
            text,
            style: TextStyle(
              color: Color(0xFF1a1824),
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
