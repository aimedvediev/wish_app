import 'package:flutter/material.dart';

class TitleHome extends StatelessWidget {
  final String text;

  const TitleHome(
    this.text,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        top: 20,
      ),
      child: Container(
        alignment: Alignment.bottomLeft,
        child: Text(
          text,
          style: TextStyle(
            color: Color(0xFFb4b4b4),
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}
