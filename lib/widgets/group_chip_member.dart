import 'package:flutter/material.dart';
import 'package:flutter_app_app/models/member.dart';

class ChipGroupMember extends StatelessWidget {
  final GROUPS groups;

  ChipGroupMember(this.groups);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(8),
      ),
      child: Container(
        alignment: Alignment.center,
        height: 32,
        width: 72,
        color: groups == GROUPS.FAMILY
            ? Color(0xFFf8b4c1)
            : groups == GROUPS.FRIENDS ? Color(0xFF8dc8a5) : Color(0xFF91bedc),
        child: Text(
          groups == GROUPS.FAMILY
              ? 'family'
              : groups == GROUPS.FRIENDS ? 'friends' : 'colleagues',
          style: TextStyle(
            color: Color(0xFFf4f1ea),
          ),
        ),
      ),
    );
  }
}
